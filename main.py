from megabus.megabus import *

if __name__ == '__main__':
    # LA code = 390, SF code = 414
    # Example: LA to SF, 15 dollar max inclusive, search 90 days, 
    # only trips from midnight until 2 PM.
    get_trips(390, 414, 15, 90, (0, 14))

    # Return trip, different filter parameters, 1 dollar ceiling, 
    # any time.
    get_trips(414, 390, 1, 90, (0, 24))