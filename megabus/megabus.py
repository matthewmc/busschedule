import requests
import datetime
import html.parser as htmlparse

week = ['Monday', 'Tuesday', 'Wednesday', \
        'Thursday', 'Friday', 'Saturday', 'Sunday']

weekdays = {i : week[i] for i in range(0, 7)}


class Trip():

    def __init__(self):
        self.time = None
        self.price = None


class findTrips(htmlparse.HTMLParser):
    
    scrape = False
    time = False
    results = []
    trip = Trip()

    def handle_starttag(self, tag, attrs):
        # Restructure the attrs from a list of 2-tuples to a dict.
        # Note to self: key collision? 
        attrs = {x[0] : x[1] for x in attrs}
        # If the data block has begun, flip the flag to start 
        # scrapping.
        if tag == 'ul':
            try:
                if attrs['class'] == 'journey standard seat':
                    self.scrape = True
            except KeyError:
                pass
        # Triggers for the different types of data.
        if tag == 'li':
            try:
                if attrs['class'] == 'two':
                    self.time = True
            except:
                pass

    def handle_endtag(self, tag):
        # Append the details struct to the results list, 
        # instantiate a new det. struct.
        if tag == 'ul' and self.scrape == True:
            self.scrape = False
            self.results.append(self.trip)
            self.trip = Trip()

        # Reset the list item flags.
        if tag == 'li' and self.time == True:
            self.time = False

    def handle_data(self, data):
        # Only if we're supposed to scrape...
        if self.scrape == True:
            # We'll consider the tag and then set the struct property
            # to the right value, with some type recasting and encoding,
            # of course
            if ('AM' in data or 'PM' in data) and self.trip.time == None:
                # Strip white space, strip bytes spacing object.
                hour = data.strip().strip(u'\xa0').split(':')
                # Handle AM data or the special case of noon.
                if 'AM' in data or ('PM' in data and hour[0] == '12'):
                    hour[0] = int(hour[0])
                    # Remember to strip off the AM or PM that is still in the string.
                    hour[1] = int(hour[1].strip('APM')) 
                # Normalize RM data to a 24hr standard.
                elif 'PM' in data:
                    hour[0] = int(hour[0]) + 12
                    hour[1] = int(hour[1].strip('PM')) 
                self.trip.time = datetime.time(hour[0], hour[1])
            # Price encountered.
            elif '$' in data:
                self.trip.price = float(data.strip().strip('$'))

    def return_results(self):
        return self.results

    def reset_results(self):
        self.results = []


def get_trips(start, dest, price=10, days=90, range_=(0, 24), passengers=1):

    today = datetime.date.today()
    day = datetime.date(today.year, today.month, today.day)
    results = {}
    parser = findTrips()
    print('Parameters: ',
          ' Price Ceiling = ', price, 
          ' Days Searched = ', days,
          ' Hour Range = {0} to {1}'.format(range_[0], range_[1]),
          )

    for i in range(0, days):
        query = 'http://us.megabus.com/JourneyResults.aspx?originCode=' + \
                str(start) + '&destinationCode=' + str(dest) + '&passengerCount=' \
                + str(passengers) + '&outboundDepartureDate=' + \
                '{0}/{1}/{2}'.format(day.month, day.day, day.year)
        trips = requests.get(query)
        parser.feed(trips.text)
        daily_res = parser.return_results()

        # Get trips below a price ceiling.
        desired_trips = [trip for trip in daily_res if trip.price <= price]
        desired_trips = [trip for trip in desired_trips 
                        if (trip.time.hour >= range_[0] and trip.time.hour < range_[1]) or
                        (trip.time.hour == range_[1] and trip.time.minute == 0)]

        # If relevant results, print the day.
        if len(desired_trips) > 0:
            print(day, weekdays[day.weekday()]) 
            print('\t', 'Number of viable trips: ', len(desired_trips))
            for trip in desired_trips:
                print('\t', 'Price: ', trip.price, 
                      '\t', 'Time: ', trip.time)
            
        # Push a day ahead.
        day = day + datetime.timedelta(days=1)

        # Clear the results in the parser.
        parser.reset_results()


if __name__ == '__main__':
    # LA code = 390, SF code = 414
    # Example: LA to SF, 15 dollar max inclusive, search 90 days, 
    # only trips from midnight until 2 PM.
    get_trips(390, 414, 1, 90, (0, 14))

    # Return trip, different filter parameters.
    get_trips(414, 390, 1, 90, (0, 24))